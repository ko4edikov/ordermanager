package org.kochedikov.order_manager.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="orders_id_seq")
    @SequenceGenerator(name="orders_id_seq", sequenceName="orders_id_seq", allocationSize=1)
    @Column (name = "id")
    private Integer id;

    @Column(name = "order_date",nullable = false)
    private Date date;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column (name = "price", nullable = false)
    private BigDecimal price;

    @Enumerated (value = EnumType.STRING)
    @Column (name = "currency")
    private Currency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "client_id")
    private Client client;



    public Order() {
    }


    public enum Status {
        OPEN,
        PROCESSED,
        COMPLETED,
        DECLINED
    }

    public enum Currency {
        UAH,
        USD,
        EUR
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

}
