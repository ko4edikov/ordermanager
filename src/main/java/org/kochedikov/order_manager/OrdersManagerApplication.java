package org.kochedikov.order_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersManagerApplication.class, args);
	}
}
