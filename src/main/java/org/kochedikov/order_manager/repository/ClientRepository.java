package org.kochedikov.order_manager.repository;

import org.kochedikov.order_manager.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer>{

}
