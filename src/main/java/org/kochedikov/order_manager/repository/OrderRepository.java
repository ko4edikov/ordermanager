package org.kochedikov.order_manager.repository;

import org.kochedikov.order_manager.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer>{

}
