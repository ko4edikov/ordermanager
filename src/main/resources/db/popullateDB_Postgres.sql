DELETE FROM orders;
DELETE FROM clients;

INSERT INTO clients (first_name, last_name,birthday, gender, ident_number)
	VALUES ('Anton','Barabash','01.01.1990','MALE',0123456789),
		('Andriy','Andrienko','01.01.1990','MALE',0123456780),
		('Pavlo','Gavrilenko','10.09.1961','MALE',9876543321),
		('Maria','Gandzuk','08.03.1970','FEMALE',000000092),
		('Abdurahman','Ibn Hattab','10.10.1982','MALE',9876543223);


INSERT INTO orders (order_date,status,price,currency,client_id)
	VALUES ('02.08.2017','PROCESSED',54.25,'USD',1),
		('03.09.2017','COMPLETED',1044.03,'UAH',2),
		('03.09.2017','COMPLETED',1044.03,'EUR',2),
		('05.09.2017','COMPLETED',99.99,'EUR',3),
		('07.09.2017','OPEN',99.99,'USD',5),
		('03.08.2017','OPEN',51.00,'UAH',4),
		('05.07.2017','DECLINED',150.00,'USD',5);