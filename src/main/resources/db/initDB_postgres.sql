-- Table: public.orders
DROP TABLE IF EXISTS public.orders;
DROP TABLE IF EXISTS public.clients;

DROP SEQUENCE IF EXISTS public.clients_id_seq;
DROP SEQUENCE IF EXISTS public.orders_id_seq;
  
CREATE SEQUENCE public.clients_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE SEQUENCE public.orders_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE public.clients
(
    id integer NOT NULL DEFAULT nextval('clients_id_seq'::regclass),
    first_name text,
    last_name text,
    birthday date,
    gender text,
    ident_number bigint,
    CONSTRAINT clients_pkey PRIMARY KEY (id),
    CONSTRAINT clients_ident_number_key UNIQUE (ident_number)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.clients
    OWNER to postgres;



CREATE TABLE public.orders
(
    id integer NOT NULL DEFAULT nextval('orders_id_seq'::regclass),
    order_date date,
    status text,
    price numeric,
    currency text,
    client_id integer,
    CONSTRAINT orders_pkey PRIMARY KEY (id),
    CONSTRAINT client FOREIGN KEY (client_id)
        REFERENCES public.clients (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.orders
    OWNER to postgres;



